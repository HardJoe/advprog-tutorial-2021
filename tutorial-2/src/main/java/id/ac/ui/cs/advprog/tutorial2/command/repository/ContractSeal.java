package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    private Spell latestSpell;
    private Map<String, Spell> spells;
    boolean hasUndone;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    /**
     * Execute next spell.
     * Set latestSpell to currentSpell for undo purposes.
     * @param spellName executed spell
     */
    public void castSpell(String spellName) {
        Spell currentSpell = spells.get(spellName);
        currentSpell.cast();
        latestSpell = currentSpell;
        hasUndone = false;
    }

    /**
     * Cannot undo again if already undid once unless previous action is cast.
     */
    public void undoSpell() {
        if (!hasUndone) {
            latestSpell.undo();
            hasUndone = true;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
