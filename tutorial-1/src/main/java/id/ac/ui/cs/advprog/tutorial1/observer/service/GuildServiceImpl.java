package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    /**
     * Since observer package initializer class doesn't exist,
     * initialization of guild and classes is done in this method.
     * @param questRepository quest repo
     */
    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        this.agileAdventurer = new AgileAdventurer(this.guild);
        this.knightAdventurer = new KnightAdventurer(this.guild);
        this.mysticAdventurer = new MysticAdventurer(this.guild);
        this.guild.add(agileAdventurer);
        this.guild.add(knightAdventurer);
        this.guild.add(mysticAdventurer);
    }

    /**
     * Assign new quest from quest form to guild's quest.
     * @param quest new quest
     */
    public void addQuest(Quest quest) {
        guild.addQuest(quest);
        questRepository.save(quest);
    }

    public List<Adventurer> getAdventurers() {
        return guild.getAdventurers();
    };
}
