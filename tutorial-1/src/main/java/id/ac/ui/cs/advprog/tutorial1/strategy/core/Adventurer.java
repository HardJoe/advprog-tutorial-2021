package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Context superclass
 */
public abstract class Adventurer {

    private AttackBehavior attackBehavior;
    private DefenseBehavior defenseBehavior;

    /**
     * Shows attackBehavior description.
     * @return description
     */
    public String attack() {
        return attackBehavior.attack();
    }

    /**
     * Shows defenseBehavior description.
     * @return description
     */
    public String defend() {
        return defenseBehavior.defend();
    }

    public Adventurer() {}

    /**
     * Return adventurer's alias (name).
     * For the sake of simplicity, adventurers are named after their
     * respective classes.
     * @return alias
     */
    public abstract String getAlias();

    public void setAttackBehavior(AttackBehavior attackBehavior) {
        this.attackBehavior = attackBehavior;
    }

    public AttackBehavior getAttackBehavior() {
        return attackBehavior;
    }

    public void setDefenseBehavior(DefenseBehavior defenseBehavior) {
        this.defenseBehavior = defenseBehavior;
    }

    public DefenseBehavior getDefenseBehavior() {
        return defenseBehavior;
    }
}
