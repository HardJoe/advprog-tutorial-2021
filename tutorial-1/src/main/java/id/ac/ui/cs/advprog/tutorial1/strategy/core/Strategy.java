package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Strategy superclass
 */
public interface Strategy {

    String getType();
}
