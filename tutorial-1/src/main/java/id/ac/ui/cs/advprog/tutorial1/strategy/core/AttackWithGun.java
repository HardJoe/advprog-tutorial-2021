package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Concrete strategy class
 */
public class AttackWithGun implements AttackBehavior {

    public String getType() {
        return "Gun";
    }

    public String attack() {
        return "DOR";
    }

    @Override
    public String toString() {
        return getType();
    }
}
