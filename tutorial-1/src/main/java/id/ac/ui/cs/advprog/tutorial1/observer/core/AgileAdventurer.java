package id.ac.ui.cs.advprog.tutorial1.observer.core;

/**
 * Concrete subscriber class
 */
public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.guild = guild;
        this.name = "Agile";
    }

    /**
     * Accept quest if type is Delivery or Rumble.
     */
    public void update() {
        if (guild.getQuestType().equals("D") || guild.getQuestType().equals("R")) {
            getQuests().add(guild.getQuest());
        }
    }
}
