package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Context subclass
 */
public class MysticAdventurer extends Adventurer {

    /**
     * Default weapons are magic and shield.
     */
    public MysticAdventurer() {
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    public String getAlias() {
        return "Mystic";
    };
}
