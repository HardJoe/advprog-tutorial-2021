package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Context subclass
 */
public class AgileAdventurer extends Adventurer {

    /**
     * Default weapons are gun and barrier.
     */
    public AgileAdventurer() {
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    public String getAlias() {
        return "Agile";
    }
}
