package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Context subclass
 */
public class KnightAdventurer extends Adventurer {

    /**
     * Default weapons are sword and armor.
     */
    public KnightAdventurer() {
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    public String getAlias() {
        return "Knight";
    };
}
