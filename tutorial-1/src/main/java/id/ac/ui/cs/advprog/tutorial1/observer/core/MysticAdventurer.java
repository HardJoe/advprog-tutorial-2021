package id.ac.ui.cs.advprog.tutorial1.observer.core;

/**
 * Concrete subscriber class
 */
public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.guild = guild;
        this.name = "Mystic";
    }

    /**
     * Accept quest if type is Delivery or Escort.
     */
    public void update() {
        if (guild.getQuestType().equals("D") || guild.getQuestType().equals("E")) {
            getQuests().add(guild.getQuest());
        }
    }
}
