package id.ac.ui.cs.advprog.tutorial1.observer.core;

/**
 * Concrete subscriber class
 */
public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.guild = guild;
        this.name = "Knight";
    }

    /**
     * Accept any of the quest types.
     */
    public void update() {
        getQuests().add(guild.getQuest());
    }
}
