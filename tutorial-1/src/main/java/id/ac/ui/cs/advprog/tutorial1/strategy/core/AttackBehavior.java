package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Strategy subclass
 */
public interface AttackBehavior extends Strategy {

    String attack();
}
