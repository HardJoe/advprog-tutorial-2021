package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Strategy subclass
 */
public interface DefenseBehavior extends Strategy {

    String defend();
}
