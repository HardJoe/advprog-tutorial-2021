package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Subscriber class
 */
public abstract class Adventurer {
    protected Guild guild;
    protected String name; // just like adventurer's class
    private List<Quest> quests = new ArrayList<>();

    /**
     * Check current quest from guild.
     * If adventurer's class is suitable for the quest type,
     * add guild's quest to the adventurer's quests.
     */
    public abstract void update();

    public String getName() {
        return this.name;
    }

    public List<Quest> getQuests() {
        return this.quests;
    }
}
