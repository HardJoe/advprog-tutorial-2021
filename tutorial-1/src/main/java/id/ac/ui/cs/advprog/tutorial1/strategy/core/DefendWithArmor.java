package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Concrete strategy class
 */
public class DefendWithArmor implements DefenseBehavior {

    public String getType() {
        return "Armor";
    }

    public String defend() {
        return "ARMOAR";
    }

    @Override
    public String toString() {
        return getType();
    }
}
