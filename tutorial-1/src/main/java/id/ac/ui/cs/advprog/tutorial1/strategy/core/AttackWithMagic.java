package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Concrete strategy class
 */
public class AttackWithMagic implements AttackBehavior {

    public String getType() {
        return "Magic";
    }

    public String attack() {
        return "ALAKAZAM";
    }

    @Override
    public String toString() {
        return getType();
    }
}
