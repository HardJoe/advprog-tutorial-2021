package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Concrete strategy class
 */
public class AttackWithSword implements AttackBehavior {

    public String getType() {
        return "Sword";
    }

    public String attack() {
        return "SHAKIN";
    }

    @Override
    public String toString() {
        return getType();
    }
}
