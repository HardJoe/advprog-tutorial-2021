package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Concrete strategy class
 */
public class DefendWithShield implements DefenseBehavior {

    public String getType() {
        return "Shield";
    }

    public String defend() {
        return "SHIEEED";
    }

    @Override
    public String toString() {
        return getType();
    }
}
