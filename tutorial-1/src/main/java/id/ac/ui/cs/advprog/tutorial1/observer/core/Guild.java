package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Publisher class
 */
public class Guild {
    private List<Adventurer> adventurers = new ArrayList<>();
    private Quest quest; // only one available quest at a time

    public void add(Adventurer adventurer) {
        adventurers.add(adventurer);
    }

    public void addQuest(Quest quest) {
        this.quest = quest;
        broadcast();
    }

    public String getQuestType() {return quest.getType();}

    public Quest getQuest() {return quest;}

    public List<Adventurer> getAdventurers() {
        return adventurers;
    }

    /**
     * When a new quest appears, and will assign quest to each adventurer.
     * Whether the quest will be accepted or not depends on the adventurer's
     * class.
     */
    private void broadcast() {
        for (Adventurer adventurer : adventurers) {
            adventurer.update();
        }
    }
}
