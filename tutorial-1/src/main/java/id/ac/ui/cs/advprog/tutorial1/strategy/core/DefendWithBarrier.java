package id.ac.ui.cs.advprog.tutorial1.strategy.core;

/**
 * Concrete strategy class
 */
public class DefendWithBarrier implements DefenseBehavior {

    public String getType() {
        return "Barrier";
    }

    public String defend() {
        return "BERRIER";
    }

    @Override
    public String toString() {
        return getType();
    }
}
