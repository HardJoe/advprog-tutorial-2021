package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    /**
     * Calculate adventurer's power based on their birth year.
     * @param birthYear birth year
     * @return power
     */
    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            rawAge *= 2000;
        } else if (rawAge <50) {
            rawAge *= 2250;
        } else {
            rawAge *= 5000;
        }
        return rawAge;
    }

    /**
     * Calculate current adventurer's age.
     * @param birthYear birth year
     * @return age
     */
    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }

    /**
     * Determine class from adventurer's power.
     * @param power power from birth year calculation
     * @return class letter
     */
    @Override
    public String decideClass(int power) {
        if (power <= 20000) {
            return "C";
        } else if (power <= 100000) {
            return "B";
        } else {
            return "A";
        }
    }
}
